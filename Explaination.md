# What is Dashgraf UI

Dashgraf UI is a CSS library designed to help with the creation of Dashgraf experiences.

It is separated into components to make delivery smaller, especially since it is designed to serve mobile content.
# Options
## All.css
All.css is one file that contains all of the other CSS files. It is useful if you use a lot of components from across Dashgraf UI, and don't want to have multiple requests.